#include "PreLoadScene.h"
#include "SimpleAudioEngine.h"
#include "GameScene.h"

using namespace CocosDenshion;
using namespace ui;
PreLoadScene::PreLoadScene()
:_sourceCount(0)
{
};

PreLoadScene::~PreLoadScene()
{

};

Scene* PreLoadScene::createScene() 
{
	auto scene = Scene::create();
	auto layer = PreLoadScene::create();
	scene->addChild(layer);
	return scene;
};

bool PreLoadScene::init() 
{
	if (!Layer::init())
	{
		return false;
	}
	Size visiblesize = Director::getInstance()->getVisibleSize();
	auto sp_bar = Sprite::create("progressbar.png");
	 _progress = ProgressTimer::create(sp_bar);
	_progress->setPercentage(0.0f);
	_progress->setMidpoint(Vec2(0.0f, 0.5f));
	_progress->setBarChangeRate(Vec2(1.0f, 0.0f));
	_progress->setType(ProgressTimer::Type::BAR);
	_progress->setPosition(visiblesize.width / 2, visiblesize.height / 2);
	this->addChild(_progress);
	return true;
};

void PreLoadScene::onEnterTransitionDidFinish() 
{
	printf("@mao 进来了");
	Layer::onEnterTransitionDidFinish();
	_progressInterval = 0;
	loadMusicAndEffect();
};

void PreLoadScene::loadMusicAndEffect() 
{
	auto callFunc = CallFunc::create([=]() 
	{	//预加载背景音乐
		SimpleAudioEngine::getInstance()->preloadBackgroundMusic("Music/bgm.mp3");
		SimpleAudioEngine::getInstance()->preloadEffect("Music/shipDestroyEffect.mp3");
		SimpleAudioEngine::getInstance()->preloadEffect("Music/mainMainMusic.mp3");
		SimpleAudioEngine::getInstance()->preloadEffect("Music/fireEffect.mp3");
		SimpleAudioEngine::getInstance()->preloadEffect("Music/buttonEffect.mp3");
	});
	auto callFunc2 = CallFunc::create([=]() 
	{
		progressUpdate();
	});
	this->runAction(Sequence::create(callFunc, callFunc2, NULL));
};

void PreLoadScene::progressUpdate() 
{
	auto progressTo = ProgressFromTo::create(1.0f, 0.0, 100);
	auto callFunc = CallFunc::create([=] 
	{
		auto delay = DelayTime::create(1.0f);
		auto callFunc = CallFunc::create([=] {
			printf("@mao 进度条 ");
			Director::getInstance()->replaceScene(GameScene::scene());
		});
		auto action = Sequence::create(progressTo, callFunc, NULL);
		_progress->runAction(action);
	});
	auto action = Sequence::create(progressTo, callFunc, NULL);
	_progress->runAction(action);
};
