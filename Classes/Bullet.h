#pragma once
#include "cocos2d.h"

using namespace cocos2d;

class Bullet :public Sprite 
{
public:
	Bullet();
	virtual ~Bullet();
	static Bullet* createWithSpriteFrame(SpriteFrame* spriteFrame);
	static Bullet* createWithSpriteFrameName(const std::string& spriteFrameName);
	bool init();
	CREATE_FUNC(Bullet);
};