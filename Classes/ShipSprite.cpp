#include "ShipSprite.h"
#include "GameScene.h"
#include "SimpleAudioEngine.h"

using namespace CocosDenshion;
ShipSprite::ShipSprite() 
:m_bCanAttack(true),
m_scaleShip(nullptr)
{
	m_life = 4;
	m_life_count = 4;
};
ShipSprite::~ShipSprite() 
{

};

bool ShipSprite::init() 
{
	if (!Sprite::init())
	{
		return false;
	};

	//飞机sprite // Vector 向量是一个能够存放任意类型的动态数组
	Vector<SpriteFrame*> shipFrame;
	shipFrame.pushBack(SpriteFrame::create(m_aniFile, Rect(59, 0, 59, 44)));
	shipFrame.pushBack(SpriteFrame::create(m_aniFile, Rect(0, 0, 59, 44)));
	Animation* ani = Animation::createWithSpriteFrames(shipFrame, 0.10);
	Animate* actAni = Animate::create(ani);

	auto act_ship = Sequence::create(actAni, CallFunc::create(CC_CALLBACK_0(ShipSprite::onBulletFrameEnd, this)), nullptr);
	this->runAction(RepeatForever::create(act_ship));





	return true;
};

ShipSprite* ShipSprite::create(const char* fileName)
{
	ShipSprite* ret = new ShipSprite;
	ret->setFrameImage(fileName);
	if (ret->init())
	{
		ret->autorelease();
		return ret;
	};
	return nullptr;
};
void ShipSprite::setFrameImage(const char* fileName) 
{
	m_aniFile = fileName;
};

void ShipSprite::onBulletFrameEnd() 
{
	GameScene* gameLayer = (GameScene*)this->getParent();
	gameLayer->createShipBullet();
	SimpleAudioEngine::getInstance()->playEffect("fireEffect.mp3", false, 1.0, 0.0, 1.0);
};