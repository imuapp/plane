#include "MenuScene.h"


#include "ui/CocosGUI.h"
#include "SimpleAudioEngine.h"
#include "PreLoadScene.h"

using namespace CocosDenshion;
using namespace ui;
USING_NS_CC;

MenuScene::MenuScene()
{
};
MenuScene::~MenuScene()
{
};
Scene* MenuScene::createScene()
{
	/*Scene* scene = Scene::create();
	MenuScene* layer = MenuScene::create();*/

	auto scene = Scene::create();
	auto layer = MenuScene::create();
	scene->addChild(layer);



	return scene;

};

bool MenuScene::init()
{
	if (!Layer::init())
	{
		return false;
	}
	Size visibleSize = Director::getInstance()->getVisibleSize();


	Sprite* planeBg = Sprite::create("planeBg.png");
	planeBg->setPosition(visibleSize / 2);
	this->addChild(planeBg);
	 
	int music = SimpleAudioEngine::getInstance()->playEffect("mainMusic.mp3", true, 1.0, 0.0, 1.0);
	SpriteFrame* btn_play1 = SpriteFrame::create("menu.png", Rect(1, 66, 125, 33));
	SpriteFrame* btn_play2 = SpriteFrame::create("menu.png", Rect(1, 33, 125, 33));
	SpriteFrame* btn_play3 = SpriteFrame::create("menu.png", Rect(1, 0, 125, 33));

	auto sp_play1 = Sprite::createWithSpriteFrame(btn_play1);
	auto sp_play2 = Sprite::createWithSpriteFrame(btn_play2);
	auto sp_play3 = Sprite::createWithSpriteFrame(btn_play3);

	auto playItem = MenuItemSprite::create(sp_play1, sp_play2, sp_play3, CC_CALLBACK_1(MenuScene::newGameCallback, this));
	playItem->setPosition(Vec2(visibleSize.width/2,visibleSize.height/2 ));


	//SpriteFrame* option_buttonFrame3 = SpriteFrame::create("menu.png", Rect(127, 66, 125, 33));
	//SpriteFrame* option_buttonFrame2 = SpriteFrame::create("menu.png", Rect(127, 33, 125, 33));
	//SpriteFrame* option_buttonFrame1 = SpriteFrame::create("menu.png", Rect(127, 0, 125, 33));
	//auto option_sprite1 = Sprite::createWithSpriteFrame(option_buttonFrame1);
	//auto option_sprite2 = Sprite::createWithSpriteFrame(option_buttonFrame2);
	//auto option_sprite3 = Sprite::createWithSpriteFrame(option_buttonFrame3);
	//auto optionItem = MenuItemSprite::create(option_sprite1, option_sprite2, option_sprite3, CC_CALLBACK_1(MenuScene::newGameCallback, this));
	//optionItem->setPosition(Vec2(visibleSize.width / 2, visibleSize.height*0.3));
	//SpriteFrame* about_buttonFrame3 = SpriteFrame::create("menu.png", Rect(253, 66, 125, 33));
	//SpriteFrame* about_buttonFrame2 = SpriteFrame::create("menu.png", Rect(253, 33, 125, 33));
	//SpriteFrame* about_buttonFrame1 = SpriteFrame::create("menu.png", Rect(253, 0, 125, 33));
	//auto about_sprite1 = Sprite::createWithSpriteFrame(about_buttonFrame1);
	//auto about_sprite2 = Sprite::createWithSpriteFrame(about_buttonFrame2);
	//auto about_sprite3 = Sprite::createWithSpriteFrame(about_buttonFrame3);
	//auto aboutItem = MenuItemSprite::create(about_sprite1, about_sprite2, about_sprite3, CC_CALLBACK_1(MenuScene::newGameCallback, this));
	//aboutItem->setPosition(Vec2(visibleSize.width / 2, visibleSize.height*0.2));

	auto menu = Menu::create(playItem, NULL);
	menu->setPosition(Vec2::ZERO);
	this->addChild(menu);
	return true;
};

void MenuScene::newGameCallback(Ref* sender)
{
	SimpleAudioEngine::getInstance()->playEffect("buttonEfffect.mp3", true, 1.0, 0.0, 1.0);
	newGmaeAnimate();
};
void MenuScene::newGmaeAnimate()
{
	Size visibleSize = Director::getInstance()->getVisibleSize();
	auto sp_ani = Sprite::create("flare.jpg");
	sp_ani->setPosition(Vec2(50, visibleSize.height*0.6));
	ccBlendFunc cbl = {GL_SRC_ALPHA, GL_ONE};
	sp_ani->setBlendFunc(cbl);
	this->addChild(sp_ani);

	auto rotate = RotateBy::create(1, 40);
	auto move = MoveBy::create(1, Vec2(visibleSize.width*0.75, 0));
	auto scale = ScaleTo::create(1, 0.7);
	auto spawn = Spawn::create(rotate, move, scale, NULL);


	auto rotate1 = RotateBy::create(1, 15);
	auto callFunc = CallFunc::create([]()
	{
		SimpleAudioEngine::getInstance()->stopAllEffects();
		Director::getInstance()->replaceScene(PreLoadScene::createScene());
	});
	//sp_ani->runAction(Sequence::create(spawn, rotate1, callFunc));
	sp_ani->runAction(Sequence::create(spawn, rotate1, callFunc,NULL));

};

