#pragma once
#include "cocos2d.h"
#include  "ShipSprite.h"


using namespace cocos2d;


class GameScene :public Layer
{
public:
	GameScene();
	~GameScene();
	virtual bool init() override;
	//static Scene* createScene();
	CREATE_FUNC(GameScene);
	static Scene* scene();
	void createShipBullet();
protected:
	Sprite* bgs[2];
	ShipSprite*  m_ship;
};
