#include "GameScene.h"
#include "Bullet.h"

GameScene::GameScene() 
:m_ship(false)
{

};

GameScene::~GameScene() 
{
	m_ship->release();
};


bool GameScene::init() 
{
	if (!Layer::init()) 
	{
		return false;
	}

	Size visiblesize = Director::getInstance()->getVisibleSize();
	bgs[0] = Sprite::create("bg01.jpg");
	bgs[1] = Sprite::create("bg01.jpg");
	bgs[0]->setAnchorPoint(Vec2(0, 0));
	bgs[1]->setAnchorPoint(Vec2(0, 0));
	bgs[0]->setPosition(Vec2(0, 0));
	bgs[1]->setPosition(Vec2(0, bgs[0]->getContentSize().height));
	this->addChild(bgs[0]);
	this->addChild(bgs[1]);
	MoveBy* actMove = MoveBy::create(14, Vec2(0, -1*bgs[0]->getContentSize().height));
	MoveBy* actMove1 = MoveBy::create(14, Vec2(0, -1*bgs[1]->getContentSize().height));
	auto act1 = Sequence::create(actMove, Place::create(Vec2(0,0)), NULL);
	auto act2 = Sequence::create(actMove1, Place::create(Vec2(0, bgs[1]->getContentSize().height)),NULL);
	bgs[0]->runAction(RepeatForever::create(act1));
	bgs[1]->runAction(RepeatForever::create(act2));

	m_ship = ShipSprite::create("ship01.png");
	m_ship->retain();
	m_ship->setPosition(Vec2(visiblesize.width / 2, 80));
	this->addChild(m_ship);









};

Scene* GameScene::scene() 
{
	Scene* ret = Scene::create();
	ret->addChild(GameScene::create());
	return ret;
};

void GameScene::createShipBullet() 
{
	Size visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 shipPos = m_ship->getPosition();
	Vec2 bullectPos_l(shipPos.x - 12, shipPos.y + 22);
	Vec2 bullectPos_r(shipPos.x + 12, shipPos.y + 22);
	Bullet* bullet_l = Bullet::createWithSpriteFrameName("W1.png");
	Bullet* bullet_r = Bullet::createWithSpriteFrameName("W1.png");
	ccBlendFunc cbl = {GL_SRC0_ALPHA, GL_ONE};

	bullet_l->setBlendFunc(cbl);
	bullet_r->setBlendFunc(cbl);
	bullet_l->setPosition(bullectPos_l);
	bullet_r->setPosition(bullectPos_r);
	this->addChild(bullet_l);
	this->addChild(bullet_r);
		



};

//Scene* GameScene::createScene() 
//{
//	auto scene = Scene::create();
//	auto layer = GameScene::create();
//	scene->addChild(layer);
//	return scene;
//};

