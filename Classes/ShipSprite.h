#pragma once
#include "cocos2d.h"

using namespace cocos2d;

class ShipSprite :public Sprite
{

public:
	ShipSprite();
	~ShipSprite();
	bool init();
	static ShipSprite* create(const char* fileName);
	
	void setFrameImage(const char* fileName);
	void onBulletFrameEnd();
protected:
	std::string  m_aniFile;

	//飞机血量
	int m_life;

	//飞机的生命数目
	int m_life_count;

	bool m_bCanAttack;

	Sprite* m_scaleShip;
};
