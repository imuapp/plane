#pragma once
#include "cocos2d.h"
USING_NS_CC;

class MenuScene :public cocos2d::Layer
{
public:
	MenuScene();
	~MenuScene();
	static Scene* createScene();
	CREATE_FUNC(MenuScene);
	void newGameCallback(Ref* sender);
	virtual bool init() override;  //  override在派生类的成员函数中使用override时，如果基类中无此函数，或基类中的函数并不是虚函数，编译器会给出相关错误信息。
	void newGmaeAnimate();
};
