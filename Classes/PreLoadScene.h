#pragma once
#include "cocos2d.h"
USING_NS_CC;

class PreLoadScene :public Layer
{
public:
	PreLoadScene();
	~PreLoadScene();
	static Scene* createScene();
	virtual bool init() override;
	CREATE_FUNC(PreLoadScene);
	virtual void onEnterTransitionDidFinish();

	void loadMusicAndEffect();
	void progressUpdate();

protected:
	int _sourceCount;

	ProgressTimer* _progress;

	float _progressInterval;

};