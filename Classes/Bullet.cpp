#include "Bullet.h"
#include "SimpleAudioEngine.h"

using namespace CocosDenshion;
Bullet::Bullet() 
{


};
Bullet::~Bullet() 
{


};

Bullet* Bullet::createWithSpriteFrame(SpriteFrame* spriteFrame) 
{
	Bullet* sprite = new(std::nothrow) Bullet();
	if (sprite && spriteFrame && sprite->initWithSpriteFrame(spriteFrame))
	{
		sprite->autorelease();
		return sprite;
	};
	return nullptr;
};

Bullet* Bullet::createWithSpriteFrameName(const std::string& spriteFrameName)
{
	SpriteFrame* frame = SpriteFrameCache::getInstance()->getSpriteFrameByName(spriteFrameName);
#if COCOS2D_DEBUG > 0
	char msg[256] = { 0 };
	sprintf(msg, "Invalid spriteFrameName:%s", spriteFrameName.c_str());
#endif
	return createWithSpriteFrame(frame);

};

bool Bullet::init() 
{
	if (!Sprite::init())
	{
		return false;
	};
	return true;
};
